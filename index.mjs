import fs from 'fs'
import fsProm from 'fs/promises'
import report from 'yurnalist'
import axios from 'axios'
import dotenv from 'dotenv'
import { VMScript } from 'vm2'
import express from 'express'
import { chromium } from 'playwright'
import { expect } from '@playwright/test'

// Setup
dotenv.config()
const sites = JSON.parse(fs.readFileSync('./sites.json'))
const siteNames = sites.map(s => s.name)
const checkInterval = process.env.CHECK_INTERVAL || 3000
const checkTemplate = `<!DOCTYPE html><html><body>
  <script type="text/javascript">
    !function () { var e = function () { var e, t = "__tcfapiLocator", a = [], n = window; for (; n;) { try { if (n.frames[t]) { e = n; break } } catch (e) { } if (true) break; } e || (!function e() { var a = n.document, r = !!n.frames[t]; if (!r) if (a.body) { var i = a.createElement("iframe"); i.style.cssText = "display:none", i.name = t, a.body.appendChild(i) } else setTimeout(e, 5); return !r }(), n.__tcfapi = function () { for (var e, t = arguments.length, n = new Array(t), r = 0; r < t; r++)n[r] = arguments[r]; if (!n.length) return a; if ("setGdprApplies" === n[0]) n.length > 3 && 2 === parseInt(n[1], 10) && "boolean" == typeof n[3] && (e = n[3], "function" == typeof n[2] && n[2]("set", !0)); else if ("ping" === n[0]) { var i = { gdprApplies: e, cmpLoaded: !1, cmpStatus: "stub" }; "function" == typeof n[2] && n[2](i) } else a.push(n) }, n.addEventListener("message", (function (e) { var t = "string" == typeof e.data, a = {}; try { a = t ? JSON.parse(e.data) : e.data } catch (e) { } var n = a.__tcfapiCall; n && window.__tcfapi(n.command, n.version, (function (a, r) { var i = { __tcfapiReturn: { returnValue: a, success: r, callId: n.callId } }; t && (i = JSON.stringify(i)), e.source.postMessage(i, "*") }), n.parameter) }), !1)) }; e()}();
    window._sp_queue = [];
    window._sp_ = {
      config: {{ spConfig }}
    };
    {{ spScript }}
  </script>
</body></html>`

// Helper
const getSafe = (fn, defaultVal) => {
  try {
    const retVal = fn()
    return retVal !== undefined ? retVal : defaultVal
  } catch (e) {
    return defaultVal
  }
}
axios.interceptors.request.use(x => {
  x.headers['x-request-startTime'] = new Date().getTime()
  return x
})
axios.interceptors.response.use(x => {
  x.headers['x-request-startTime'] = x.config.headers['x-request-startTime']
  x.headers['x-request-endTime'] = new Date().getTime()
  return x
})

// Logic
const checkSiteCdn = async (site) => {
  report.info(`>> Checking CDN of ${site.name}`)
  const r = await axios(site.scriptUrl)
    .catch(e => {
      return { connectionException: true }
    })
  site.checks = site.checks || {}
  site.checks.cdn = {
    checkDate: new Date().getTime(),
    connected: r.connectionException !== true,
    httpCode: getSafe(() => r.status, 599),
    size: getSafe(() => r.data.length, -1),
    type: getSafe(() => r.headers['content-type'], 'undefined'),
    age: getSafe(() => r.headers['age'], 999999),
    body: getSafe(() => r.data, ''),
    start: getSafe(() => r.headers['x-request-startTime'], -1),
    end: getSafe(() => r.headers['x-request-endTime'], 999999),
    durationInMs: getSafe(() => r.headers['x-request-endTime'] - r.headers['x-request-startTime'], 999999),
  }
}
const checkApi = async (site) => {
  const script = getSafe(() => site.checks.cdn.body)
  if (!script) {
    return report.warn(`>> Skipping API-Check for ${site.name} because script is missing`)
  }

  await fsProm.writeFile(
    `./tmp/${site.name}.html`,
    checkTemplate
      .replace('{{ spScript }}', script)
      .replace('{{ spConfig }}', JSON.stringify(site.spConfig))
  )

  let passed = true
  const start = new Date().getTime()

  try {
    const browser = await chromium.launch()
    const page = await browser.newPage()
    await page.goto(`http://localhost:${port}/${site.name}.html`)
    await expect(page.locator('[id^=sp_message_container]')).toBeVisible()
    await page.screenshot({ path: `./tmp/${site.name}.png` })
    await browser.close()
  } catch(e) {
    passed = false
  }

  site.checks = site.checks || {}
  site.checks.api = {
    checkDate: new Date().getTime(),
    passed,
    durationInMs: new Date().getTime() - start,
  }
}
const checkScript = async (site) => {
  const script = getSafe(() => site.checks.cdn.body)
  if (!script) {
    return report.warn(`>> Skipping Parse-Check for ${site.name} because script is missing`)
  }

  site.checks = site.checks || {}
  site.checks.script = {
    checkDate: new Date().getTime(),
    parseable: true,
  }

  report.info(`>> Checking Script for syntax-errors of ${site.name}`)

  try {
    new VMScript(script).compile()
  } catch (err) {
    console.log(err)
    site.checks.script.parseable = false
  }
}
const checkSite = async (site) => {
  report.info(`> Kicking off ${site.name}`)
  await Promise.all([
    checkSiteCdn(site),
    checkScript(site),
    checkApi(site)
  ])
}
const checkSites = async (sites) => {
  await Promise.all(sites.map(s => checkSite(s)))
}

// Output
const format = (req, res) => {
  res.setHeader('content-type', 'text/plain')
  res.send(sites.map(s => {
    return `cdn_http_code{site=${s.name}} ${getSafe(() => s.checks.cdn.httpCode, 500)}
cdn_size{site=${s.name}} ${getSafe(() => s.checks.cdn.size, -1)}
cdn_content_type_ok{site=${s.name}} ${getSafe(() => s.checks.cdn.type) === 'application/javascript' ? 1 : 0}
cdn_download_time_ms{site=${s.name}} ${getSafe(() => s.checks.cdn.durationInMs, 99999)}
cdn_ms_since_last_check{site=${s.name}} ${getSafe(() => new Date().getTime() - s.checks.cdn.checkDate, 99999)}
cdn_age{site=${s.name}} ${getSafe(() => s.checks.cdn.age, 99999999999)}
parse_ms_since_last_check{site=${s.name}} ${getSafe(() => new Date().getTime() - s.checks.script.checkDate, 99999)}
parse_parsed{site=${s.name}} ${getSafe(() => s.checks.script.parseable) ? 1 : 0}
api_ms_since_last_check{site=${s.name}} ${getSafe(() => new Date().getTime() - s.checks.api.checkDate, 99999)}
api_passed{site=${s.name}} ${getSafe(() => s.checks.api.passed) ? 1 : 0}
api_test_ms{site=${s.name}} ${getSafe(() => s.checks.api.durationInMs, 999999)}`
  }).join('\n'))
}
const server = express()
server.use(express.static('tmp'));
server.get('/', (req, res) => res.send('Check out <a href="/metrics">/metrics</a> instead.'))
server.get('/metrics', format)

// Kick-Off
const port = process.env.PORT || 3008
server.listen(port, () => {
  report.info(`> Started the server on http://localhost:${port}/metrics`)
})
report.info(`> Checking every ${checkInterval}ms which can be overriden via $CHECK_INTERVAL`)
report.info(`> Starting checks for [${siteNames.join(', ')}]`)
setInterval(() => checkSites(sites), checkInterval)
