# SP-Checker

Checks whether or not sourcepoint is operating as it should for various platforms.

Comes with:
- Support for multiple Bertelsmann-Sites
- Checks for the CDN (response-time, HTTP-Code, length, response-type)
- Checks if the script can be parsed/compiled
- Checks whether or not the API is able to show an overlay and how long it takes
- Outputs the results in a prometheus-readable way

## How

### Add Sites

Just add your site to `sites.json` with the appropiate settings.

### Enpoints

#### [/metrics](https://spcheck.preview.wetter.de/metrics)

This is a prometheus-readable representation of the latest results per site.

#### [/site.html](https://spcheck.preview.wetter.de/wetter.html)

This is what we use to test the api internally. You should see the consent-prompt.

#### [/site.png](https://spcheck.preview.wetter.de/wetter.png)

This is a screenshot from the last api-run

