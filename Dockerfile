FROM mcr.microsoft.com/playwright:v1.21.0-focal

# Bundle APP files
COPY index.mjs package.json package-lock.json package-lock.json sites.json ./

# Install app dependencies
ENV NPM_CONFIG_LOGLEVEL warn
RUN npm ci
RUN npx playwright install --with-deps

EXPOSE 3008

CMD [ "node", "index.mjs" ]
